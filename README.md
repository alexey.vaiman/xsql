# xsql

The project addresses a rare problem of working with dynamic sql queries. Sometimes it is necessary to have a query (for some report usually) with a possibility to customize selection list, dimensions, conditions or change order. With plain sql it can be painful: plain-text sql query is not structured so adding something in the middle of query is neither easy nor nice. So my way is following:
1. manually convert plain-text sql query to xml format (_xsql_),
2. (optional) programmatically add or remove proper nodes in proper locations of xsql,
3. **transform** xsql to plain-text sql using **xsl template** [sql.xslt](https://gitlab.com/alexey.vaiman/xsql/-/blob/master/sql.xslt?ref_type=heads) in one call to `stylesheet.apply` method.

Look into [index.js](https://gitlab.com/alexey.vaiman/xsql/-/blob/master/index.js?ref_type=heads) for sample usage.

This approach had been used in production, all the manipulations were performed in Oracle PL/SQL stored procedures. I lost that template so now I'm trying to resurrect it with js lib for xslt — [node-libxslt](https://github.com/albanm/node-libxslt)

Key part is [sql.xslt](https://gitlab.com/alexey.vaiman/xsql/-/blob/master/sql.xslt?ref_type=heads) template file.

## XSQL

<div style="display:flex;flex-direction:column;max-width:70%">
<div style="flex:1;">

### Example 1
</div>
<div style="display:flex;flex:1;">
<div style="flex:1;max-width:50%;">

#### SQL

</div>
<div style="flex:0;min-width:1rem;">&nbsp;</div>
<div style="flex:1;max-width:50%;">

#### XSQL

</div>
</div>

<div style="display:flex;flex:1;">
<div style="flex:1;max-width:50%;">

    WITH employee_ranking AS (
        SELECT
            employee_id,
            last_name,
            first_name,
            salary,
            RANK() OVER (ORDER BY salary DESC) as ranking
        FROM employee
    )
    SELECT
        employee_id,
        last_name,
        first_name,
        salary
    FROM employee_ranking
    WHERE ranking <= 5
    ORDER BY ranking

</div>
<div style="flex:0;min-width:1rem;">&nbsp;</div>
<div style="flex:1;max-width:50%;">

    <select>
        <with as="employee_ranking" alias="employee_id,last_name,first_name,salary,ranking">
            <select>
                <val expr="employee_id,last_name,first_name,salary"/>
                <val expr="RANK() OVER (ORDER BY salary DESC) as ranking"/>
                <table name="employee"/>
            </select>
        </with>
        <val expr="employee_id, last_name, first_name, salary"/>
        <table name="employee_ranking"/>
        <condition expr="ranking &lt;= 5"/>
        <order by="ranking"/>
    </select>

</div>
</div>
</div>

<div style="display:flex;flex-direction:column;max-width:70%">
<div style="flex:1;">

### Example 2
</div>

<div style="display:flex;flex:1;">
<div style="flex:1;max-width:50%;">

#### SQL
</div>
<div style="flex:0;min-width:1rem;">&nbsp;</div>
<div style="flex:1;max-width:50%;">

#### XSQL
</div>
</div>

<div style="display:flex;flex:1;">
<div style="flex:1;max-width:50%;">

    SELECT
        first_name,
        last_name,
        salary
    FROM employee e1
    WHERE salary >
        (
            SELECT AVG(salary)
            FROM employee e2
            WHERE e1.departmet_id = e2.department_id
        ) + 10
</div>
<div style="flex:0;min-width:1rem;">&nbsp;</div>
<div style="flex:1;max-width:50%;">

    <select>
        <val expr="last_name, first_name, salary"/>
        <table name="employee" as="e1"/>
        <condition>
            salary &gt;
            <select> 
                <val expr="AVG(salary)"/>
                <table name="employee e2"/>
                <where expr="e1.departmet_id = e2.department_id"/>
            </select> + 10
        </condition>
    </select>

</div>
</div>
</div>

<div style="display:flex;flex-direction:column;max-width:70%">
<div style="flex:1;">

### Example 3
</div>

<div style="display:flex;flex:1;">
<div style="flex:1;max-width:50%;">

#### SQL

</div>
<div style="flex:0;min-width:1rem;">&nbsp;</div>
<div style="flex:1;max-width:50%;">

#### XSQL

</div>
</div>

<div style="display:flex;flex:1;">
<div style="flex:1;max-width:50%;">

    SELECT 
        employee_id,
        last_name,
        first_name,
        dept_id,
        manager_id,
        salary - (select AVG(salary) from employee e2 where e1.departmet_id = e2.department_id) as diff
    FROM employee
    GROUP BY   
        employee_id,
        last_name,
        first_name,
        dept_id,
        manager_id,
        salary
    HAVING COUNT(*) > 1 and max(salary) > 1000 and
        min(salary) < (
            SELECT AVG(salary)
            FROM employee e2
            WHERE e1.departmet_id = e2.department_id
        )

</div>
<div style="flex:0;min-width:1rem;">&nbsp;</div>
<div style="flex:1;max-width:50%;">

    <select>
        <table name="employee" as="e"/>
        <dim expr="employee_id"/>
        <dim expr="last_name"/>
        <dim expr="first_name"/>
        <dim expr="dept_id"/>
        <dim expr="manager_id"/>
        <dim by="salary" as="diff">
            salary - <select> 
            <val expr="AVG(salary)"/>
            <table name="employee e2"/>
            <where expr="e1.departmet_id = e2.department_id"/>
        </select>
        </dim>
        <having expr="count(*) &gt; 1"/>
        <having expr="max(salary) &gt; 1000"/>
        <having>
            min(salary) &lt; 
            <select> 
                <val expr="AVG(salary)"/>
                <table name="employee e2"/>
                <where expr="e1.departmet_id = e2.department_id"/>
            </select>
        </having>
    </select>

</div>
</div>
</div>
