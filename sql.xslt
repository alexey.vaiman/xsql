<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" omit-xml-declaration="yes" indent="yes"/>
    <xsl:strip-space elements="*"/>

    <xsl:param name="level" select="''"/>
    <xsl:param name="ident" select="'  '"/>
    <xsl:param name="depth" select="0"/>

<!-- possible top clauses -->
    <xsl:template match="select">
        <xsl:param name="level" select="''"/>
        <xsl:param name="depth" select="0"/>
        <xsl:call-template name="subquery">
            <xsl:with-param name="level" select="concat($level,$ident)" />
            <xsl:with-param name="depth" select="$depth + 1" />
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="union|unionall|minus|intersect">
        <xsl:param name="level" select="''"/>
        <xsl:param name="depth" select="0"/>
        <xsl:call-template name="setoperation">
            <xsl:with-param name="level" select="concat($level,$ident)" />
            <xsl:with-param name="depth" select="$depth + 1" />
        </xsl:call-template>
    </xsl:template>

<!-- boolean operation on queries -->
    <xsl:template name="setoperation">
        <xsl:param name="level" select="''"/>
        <xsl:param name="depth" select="0"/>
        <!-- union|unionall|intersect|minus -->
        <!-- if this is internal boolean clause (inside parent boolean set clause) then put brackets around
                open bracket in the beginning -->
        <xsl:choose>
            <xsl:when test="$depth > 2">
                <xsl:value-of select="concat($level,$ident)"/><xsl:text>(&#10;</xsl:text>
            </xsl:when>
        </xsl:choose>
        <!-- render first argument of boolean set operation -->
        <xsl:apply-templates select="*[1]">
            <xsl:with-param name="level" select="$level"/>
            <xsl:with-param name="depth" select="$depth" />
        </xsl:apply-templates>
        <!-- render set operation keyword -->
        <xsl:value-of select="concat($level,$ident)"/>
        <xsl:choose>
            <xsl:when test="name()=unionall"><xsl:text>union all&#10;</xsl:text></xsl:when>
            <xsl:otherwise><xsl:value-of select="name()"/><xsl:text>&#10;</xsl:text></xsl:otherwise>
        </xsl:choose>
        <!-- render second argument of boolean set operation -->
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="level" select="$level" />
            <xsl:with-param name="depth" select="$depth" />
        </xsl:apply-templates>
        <!-- if this is internal boolean clause (inside parent boolean set clause) then put brackets around
                close bracket in the end -->
        <xsl:choose>
            <xsl:when test="$depth > 2">
                <xsl:value-of select="concat($level,$ident)"/><xsl:text>)&#10;</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- common subquery: table, internal select, boolean set operator -->
    <xsl:template name="subquery">
        <xsl:param name = "level" select="''"/>
        <xsl:param name = "depth" select="0"/>
        <xsl:param name = "ignoreas" select="false()"/>

        <xsl:for-each select="with">
            <xsl:if test="position()=1"><xsl:value-of select="$level"/><xsl:text>with&#10;</xsl:text></xsl:if>
            <xsl:if test="position()&gt;1"><xsl:value-of select="$level"/><xsl:text>,&#10;</xsl:text></xsl:if>
            <xsl:value-of select="$level"/>
            <xsl:value-of select="@as"/>
            <xsl:if test="@alias"><xsl:text> (</xsl:text><xsl:value-of select="@alias"/><xsl:text>)</xsl:text></xsl:if>
            <xsl:text> as (&#10;</xsl:text>
            <xsl:apply-templates select="*">
                <xsl:with-param name="level" select="concat($level,$ident)"/>
                <xsl:with-param name="depth" select="$depth + 1" />
                <xsl:param name = "ignoreas" select="true()"/>
            </xsl:apply-templates>
            <xsl:value-of select="$level"/><xsl:text>)&#10;</xsl:text>
        </xsl:for-each>

        <!-- render select keyword with possible 'distinct' -->
        <!-- TODO 'all', 'unique' -->
        <xsl:value-of select="$level"/>
        <xsl:text>select&#10;</xsl:text>
        <xsl:if test="@distinct='true'"><xsl:value-of select="concat($level,$ident)"/><xsl:text>distinct&#10;</xsl:text></xsl:if>

        <!-- parts of query-block: with_clause, select-list, table-refs or join clauses, where_clause, hierarchical_query_clause (not yet), group_by_clause and order_by_clause -->
        <!-- render select list -->
        <xsl:for-each select="val|dim">
            <!-- put dimensions after values — not sure if it is necessary -->
            <xsl:sort order="descending" select="name()"/>
            <xsl:choose>
                <xsl:when test="name()='val'">
                    <xsl:call-template name="val">
                        <xsl:with-param name="level" select="concat($level,$ident)" />
                    </xsl:call-template>
                </xsl:when>
                <!-- dimensions are rendered twise - as part of select list and part of group by list
                here is select list part -->
                <xsl:when test="name()='dim'">
                    <xsl:call-template name="dim-val">
                        <xsl:with-param name="level" select="concat($level,$ident)" />
                    </xsl:call-template>
                </xsl:when>
            </xsl:choose>
            <!-- naive separators: if this is not last expression in select list then put comma after -->
            <xsl:choose>
                <xsl:when test="position()&lt;last()"><xsl:text>,&#10;</xsl:text></xsl:when>
                <xsl:otherwise><xsl:text>&#10;</xsl:text></xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>

        <!-- render table refs, join clauses or set operations -->
        <xsl:for-each select="table|select|union|minus|intersect">
        <!-- separator is rendered BEFORE next node, (not like in select list) -->
            <xsl:choose>
                <!-- first node - add 'from' before -->
                <xsl:when test="position()=1"><xsl:value-of select="$level"/><xsl:text>from&#10;</xsl:text></xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                    <!-- if there is join attribute, then NO comma needed else put comma (to separate tables or subqueries without join) -->
                        <xsl:when test="@join"><xsl:text>&#10;</xsl:text></xsl:when>
                        <xsl:otherwise><xsl:text>,&#10;</xsl:text></xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
                <!-- table renders join inside its template -->
                <xsl:when test="name()='table'">
                    <xsl:call-template name="table">
                        <xsl:with-param name="level" select="concat($level,$ident)" />
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <!-- render join clause for subqueries other than 'table' -->
                    <xsl:if test="@join">
                        <xsl:value-of select="@join"/><xsl:text> join </xsl:text>
                    </xsl:if>
                    <xsl:choose>
                        <!-- render internal select by calling subquery template recursively -->
                        <xsl:when test="name()='select'">
                            <xsl:value-of select="concat($level,$ident)"/><xsl:text>(&#10;</xsl:text>
                            <xsl:call-template name="subquery">
                                <xsl:with-param name="level" select="concat($level,$ident)" />
                                <xsl:with-param name="depth" select="$depth + 1" />
                            </xsl:call-template>
                            <xsl:value-of select="concat($level,$ident)"/><xsl:text>)</xsl:text>
                            <xsl:if test="@as"><xsl:text> as </xsl:text><xsl:value-of select="@as"/></xsl:if>
                        </xsl:when>
                        <!-- render internal set operator - add brackets around and call its template -->
                        <!-- if there are nested set operators - brackets added in template -->
                        <xsl:when test="name()='union' or name()='minus' or name()='intersect'">
                            <xsl:value-of select="concat($level,$ident)"/><xsl:text>(&#10;</xsl:text>
                            <xsl:call-template name="setoperation">
                                <xsl:with-param name="level" select="concat($level,$ident)" />
                                <xsl:with-param name="depth" select="$depth + 1" />
                            </xsl:call-template>
                            <xsl:choose>
                                <!-- add 'as' when present -->
                                <xsl:when test="@as">
                                    <xsl:value-of select="concat($level,$ident)"/><xsl:text>) as </xsl:text><xsl:value-of select="@as"/><xsl:text>&#10;</xsl:text>
                                </xsl:when>
                                <xsl:otherwise><xsl:value-of select="concat($level,$ident)"/><xsl:text>)&#10;</xsl:text></xsl:otherwise>
                            </xsl:choose>
                        </xsl:when>
                    </xsl:choose>
                    <!-- for join clause in subqueries other than 'table' render final on conditioin if present !CHECK! -->
                    <xsl:if test="@on and @join">
                        <xsl:text> on </xsl:text><xsl:value-of select="@on" disable-output-escaping="yes"/><xsl:text>&#10;</xsl:text>
                    </xsl:if>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:if test="position()=last()"><xsl:text>&#10;</xsl:text></xsl:if>
        </xsl:for-each>

        <!-- render where clause -->
        <!-- TODO add 'and' and 'or' blocks -->
        <xsl:for-each select="condition|where">
            <xsl:if test="position()=1"><xsl:value-of select="$level"/><xsl:text>where&#10;</xsl:text></xsl:if>
            <xsl:if test="position()!=1"><xsl:text> and&#10;</xsl:text></xsl:if>
            <xsl:call-template name="condition">
                <xsl:with-param name="level" select="concat($level,$ident)" />
            </xsl:call-template>
            <xsl:if test="position()=last()"><xsl:text>&#10;</xsl:text></xsl:if>
        </xsl:for-each>

        <!-- render group by clause by selecting again 'dim' elements but use different template for them -->
        <xsl:for-each select="dim">
            <xsl:if test="position()=1"><xsl:value-of select="$level"/><xsl:text>group by&#10;</xsl:text></xsl:if>
            <xsl:call-template name="dim-group">
                <xsl:with-param name="level" select="concat($level,$ident)" />
            </xsl:call-template>
            <!-- usual tail separators - comma for all but last element in foreach iteration -->
            <xsl:choose>
                <xsl:when test="position()&lt;last()"><xsl:text>,&#10;</xsl:text></xsl:when>
                <xsl:otherwise><xsl:text>&#10;</xsl:text></xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>

        <xsl:for-each select="having">
            <xsl:if test="position()=1"><xsl:value-of select="$level"/><xsl:text>having&#10;</xsl:text></xsl:if>
            <xsl:if test="position()!=1"><xsl:text> and&#10;</xsl:text></xsl:if>
            <xsl:call-template name="condition">
                <xsl:with-param name="level" select="concat($level,$ident)" />
            </xsl:call-template>
            <xsl:if test="position()=last()"><xsl:text>&#10;</xsl:text></xsl:if>
        </xsl:for-each>

        <!-- render order by clause -->
        <xsl:for-each select="order">
            <xsl:if test="position()=1"><xsl:value-of select="$level"/><xsl:text>order by&#10;</xsl:text></xsl:if>
            <xsl:call-template name="order">
                <xsl:with-param name="level" select="concat($level,$ident)" />
            </xsl:call-template>
            <xsl:choose>
                <xsl:when test="position()&lt;last()">
                    <xsl:text>,&#10;</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>&#10;</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
            <!-- <xsl:choose>
                <xsl:when test="position()&lt;last()">
                    <xsl:text>,&#10;</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>&#10;</xsl:text>
                </xsl:otherwise>
            </xsl:choose> -->
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="table">
        <xsl:param name = "level" />
        <xsl:value-of select="$level"/>
        <xsl:choose>
            <xsl:when test="@join">
                <xsl:value-of select="@join"/>
                <xsl:text> join </xsl:text>
                <xsl:value-of select="@name"/>
                <xsl:if test="@as">
                    <xsl:text> as </xsl:text>
                    <xsl:value-of select="@as"/>
                </xsl:if>
                <xsl:if test="@on">
                    <xsl:text> on </xsl:text><xsl:value-of select="@on" disable-output-escaping="yes"/>
                </xsl:if>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="@name"/>
                <xsl:if test="@as">
                    <xsl:text> as </xsl:text>
                    <xsl:value-of select="@as"/>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- template for rendeing select list element -->
    <xsl:template name="val">
        <xsl:param name = "level" />
        <xsl:param name="level" select="$ident" />
        <xsl:param name="depth" select="1" />
        <xsl:value-of select="$level"/>
        <xsl:call-template name="render-expr-or-text">
            <xsl:with-param name="level" select="$level" />
            <xsl:with-param name="depth" select="$depth" />
        </xsl:call-template>

        <xsl:if test="@as">
            <xsl:text> as </xsl:text>
            <xsl:value-of select="@as"/>
        </xsl:if>
    </xsl:template>

    <!-- template for 'where' condition -->
    <xsl:template name="condition">
        <xsl:param name="level" select="$ident" />
        <xsl:param name="depth" select="1" />
        <xsl:call-template name="render-expr-or-text">
            <xsl:with-param name="level" select="$level" />
            <xsl:with-param name="depth" select="$depth" />
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="render-expr-or-text">
        <!-- <xsl:value-of select="name()"/> -->
        <xsl:param name="level" select="$ident" />
        <xsl:value-of select="$level"/>
        <xsl:choose>
            <xsl:when test="@expr"><xsl:value-of select="@expr" disable-output-escaping="no"/></xsl:when>
            <xsl:otherwise>
                <xsl:text>( </xsl:text>
                <xsl:for-each select="*|text()" >
                    <xsl:if test="name()=''">
                        <!-- <xsl:text>TEXT</xsl:text> -->
                        <xsl:value-of select="normalize-space(.)"/>
                    </xsl:if>
                    <xsl:if test="name()">
                        <!-- <xsl:value-of select="concat($level,$ident)"/> -->
                        <xsl:text> (&#10;</xsl:text>
                        <xsl:apply-templates select=".">
                            <xsl:with-param name="level" select="concat($level,$ident)" />
                            <xsl:with-param name="depth" select="$depth + 1" />
                        </xsl:apply-templates>
                        <xsl:value-of select="concat($level,$ident)"/><xsl:text>) </xsl:text>
                        <!-- <xsl:if test="@as"><xsl:text> as </xsl:text><xsl:value-of select="@as"/></xsl:if> -->
                    </xsl:if>
                </xsl:for-each>
                <xsl:text> )&#10;</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- template for order element -->
    <xsl:template name="order">
        <xsl:param name = "level" />
        <xsl:value-of select="$level"/>
        <xsl:value-of select="@by"/>
    </xsl:template>

    <!-- template for select list part of dimension element - same as 'val' -->
    <xsl:template name="dim-val">
        <xsl:param name="level" select="$ident" />
        <xsl:param name="depth" select="1" />
        <xsl:call-template name="render-expr-or-text">
            <xsl:with-param name="level" select="$level" />
            <xsl:with-param name="depth" select="$depth" />
        </xsl:call-template>
        <xsl:if test="@as">
            <xsl:text> as </xsl:text>
            <xsl:value-of select="@as"/>
        </xsl:if>
    </xsl:template>

    <!-- template for group-by part of dimension element -->
    <!-- TODO check if 'by' part also needs to be render-expr-or-text -->
    <xsl:template name="dim-group">
        <xsl:param name="level" select="$ident" />
        <xsl:param name="depth" select="1" />
        <xsl:value-of select="$level"/>
        <xsl:choose>
            <!-- if group by expression differs from select list expression, use it in group by list else use same 'expr' -->
            <xsl:when test="@by"><xsl:value-of select="@by"/></xsl:when>
            <xsl:when test="@expr"><xsl:value-of select="@expr"/></xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="render-expr-or-text">
                    <xsl:with-param name="level" select="$level" />
                    <xsl:with-param name="depth" select="$depth" />
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
        <!-- <xsl:text>{</xsl:text><xsl:value-of select="name()"/><xsl:tex>-</xsl:tex><xsl:value-of select="$depth"/><xsl:text>}</xsl:text> -->
        <!-- <xsl:text> (</xsl:text><xsl:value-of select="$depth"/><xsl:text>)</xsl:text> -->
        <!-- <xsl:text>¹«</xsl:text><xsl:value-of select="$level"/><xsl:value-of select="$depth"/><xsl:text>»&#10;</xsl:text> -->
        <!-- <xsl:value-of select="local-name(*[1])"/> -->

</xsl:stylesheet>
