var libxslt = require('libxslt');
var libxmljs = libxslt.libxmljs;
const fs = require('fs');
const { exit } = require('process');

var xmlDoc = libxmljs.parseXml(`
<select>
    <table name="employee" as="e"/>
    <dim expr="employee_id"/>
    <dim expr="last_name"/>
    <dim expr="first_name"/>
    <dim expr="dept_id"/>
    <dim expr="manager_id"/>
    <dim by="salary" as="diff">
        salary - <select> 
        <val expr="AVG(salary)"/>
        <table name="employee e2"/>
        <where expr="e1.departmet_id = e2.department_id"/>
    </select>
    </dim>
    <having expr="count(*) &gt; 1"/>
    <having expr="max(salary) &gt; 1000"/>
    <having>
        min(salary) &lt; 
        <select> 
            <val expr="AVG(salary)"/>
            <table name="employee e2"/>
            <where expr="e1.departmet_id = e2.department_id"/>
        </select>
    </having>
</select>
`);

const templateTxt = fs.readFileSync('sql.xslt');
const template = libxmljs.parseXml(templateTxt.toString());
if (!template) exit(0);
const stylesheetTxt = templateTxt.toString();
const stylesheet = libxslt.parse(stylesheetTxt);
const res = stylesheet.apply(xmlDoc, {"ident": "    "}, { outputFormat: "string" });

console.log(res);

//debugger;